#include "TROOT.h"
#include "TH1.h"
#include <cstdlib>
using namespace std;

Double_t GetSeparation(TH1 *S, TH1 *B);

string VectorToString(std::vector<string> v_str);
