#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cstdlib>
#include <TFile.h>
#include <TChain.h>
#include <TList.h>
#include <TString.h>
#include "TMVA/Factory.h"
#include "TMVA/DataLoader.h"
#include "TMVA/Tools.h"
#include "TMVA/TMVAGui.h"

// #include "Factory.h"
// #include "DataLoader.h"
// #include "Tools.h"
// #include "TMVAGui.h"
//
//

using namespace std;


float train(int kfold = 0);

// base function for training
std::vector<float> TrainBase(int kfold, int step, string method, std::vector<string> variable_list, bool store);
